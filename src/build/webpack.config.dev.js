const config = require('./config.js')
const webpack = require('webpack')

module.exports = {
    devServer: {
        compress: false,
        publicPath: `${config.env.host}:${config.env.port}/`,
        watchContentBase: true,
        contentBase: config.paths.dist,
        host: config.env.host,
        port: config.env.port,
        stats: 'errors-only',
        hot: true,
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(), // Enable HMR
        new webpack.NamedModulesPlugin()
    ]
}
