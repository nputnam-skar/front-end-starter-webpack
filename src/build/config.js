const path = require('path')
const { argv } = require('yargs')
const isProduction = !!argv.p
const rootPath =  process.cwd()

const config = {
    env: {
        production: isProduction,
        development: !isProduction,
        host: 'localhost',
        port: '8000'
    },
    publicPath: './',
    filenames: isProduction ? '[name]_[hash:8]' : '[name]',
    paths: {
        root: rootPath,
        dist: path.join(rootPath, 'dist'),
        src : path.join(rootPath, 'src')
    },
    enabled: {
        sourceMaps: !isProduction,
    },
    entry:  {
        main: [
            './index.hbs',
            './scss/main.scss',
            './js/main.js'
        ],
    },
    externals : {
        //jquery: 'jQuery', //if i do this i need to load via a cdn in my html file
    }
}

module.exports = config
