//webpack plugins
const webpack                   = require('webpack')
const HtmlWebpackPlugin         = require('html-webpack-plugin')
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin')
const CleanWebpackPlugin        = require('clean-webpack-plugin')
const ExtractTextPlugin         = require('extract-text-webpack-plugin')
//utilites
const merge                     = require('webpack-merge')
const autoprefixer              = require('autoprefixer')
const TodoWebpackPlugin         = require('todo-webpack-plugin')
//config
const config                    = require('./config.js')



let webpackConfig = {
    entry: config.entry,
    context: config.paths.src,
    devtool: config.enabled.sourceMaps ? 'source-map' : false,
    stats: {//see https://webpack.js.org/configuration/stats/
        assets      : false,
        children    : false,
        chunks      : false,
        hash        : false,
        modules     : false,
        publicPath  : false,
        version     : false,
        errors      : true,
        errorDetails: true,
        timings     : false,
        warnings    : true,
    },
    output: {
        path: config.paths.dist,
        publicPath: config.publicPath,
        filename: `scripts/${config.filenames}.js`,
    },
    module: {
        rules: [
            {
                test: /\.hbs$/,
                loader: 'handlebars-loader',
                query: { inlineRequires: '/images/' }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015'],
                        cacheDirectory: config.env.development,
                        minified: config.env.production,
                    },
                },
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract({
                    fallback: 'style-loader', // The backup style loader
                    publicPath: '../',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                importLoaders: 1 ,
                                sourceMap: false,
                            },
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: () => [
                                    autoprefixer(),
                                ],
                                sourceMap: true,
                            },
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true,
                            },
                        },
                    ],
                }),
            },
            {
                test: /\.css$/,
                include: config.paths.src,
                use: ExtractTextPlugin.extract({
                    fallback: 'style',
                    publicPath: '../',
                    use: [
                        'css?sourceMap=false',
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: () => autoprefixer(),
                                sourceMap: false,
                            },
                        },
                    ],
                }),
            },
            {
                test: /\.(png|jpe?g|gif|svg|ico|ttf|eot|woff)$/,
                include: config.paths.src,
                loader: 'file-loader',
                options: {
                    name: `[path]${config.filenames}.[ext]`,
                },
            },
            {
                test: /\.(ttf|eot|woff2?|png|jpe?g|gif|svg)$/,
                include: /node_modules|bower_components/,
                loader: 'file-loader',
                options: {
                    name: `vendor/${config.filenames}.[ext]`,
                },
            }
        ],
    },
    plugins: [
        new webpack.ProvidePlugin({//Creates global variables without need of import statement
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Tether: 'tether',
            'window.Tether': 'tether',
        }),
        new webpack.NoEmitOnErrorsPlugin(),
        new HtmlWebpackPlugin({
            alwaysWriteToDisk: true,
            template: './index.hbs'
        }),
        new HtmlWebpackHarddiskPlugin(),
        new CleanWebpackPlugin('dist', {
            verbose: false,
            root: config.paths.root
        }),
        new ExtractTextPlugin({ //disabled during development
            filename: `styles/${config.filenames}.css`,
            allChunks: true,
            disable: config.env.development,
        }),
        new TodoWebpackPlugin({
            console:  config.env.production,
            tags:[
                'TODO',
                'FIXME',
                'CHANGED',
                'NOTE'
            ],
            suppressFileOutput: false,
            withInlineFiles:    false ,
        })
    ],
    externals: config.externals,
}

if( config.env.development ){
    console.log('DEVELOPMENT SERVER BUILD')
    webpackConfig = merge(webpackConfig, require('./webpack.config.dev'))
} else {
    console.log('PRODUCTION SERVER BUILD')
}
module.exports = webpackConfig
