module.exports = {
    "env": {
		"node": true,
        "browser": true,
        "commonjs": true,
        "es6": true,
		"jquery": true
    },
    "extends": "eslint:recommended",
	"parserOptions": {
      "ecmaFeatures": {
        "globalReturn": true,
        "generators": false,
        "objectLiteralDuplicateProperties": false,
        "experimentalObjectRestSpread": true
      },
      "ecmaVersion": 2017,
      "sourceType": "module"
    },
    "plugins": [
      "import",
    ],
    "settings": {
      "import/core-modules": [],
      "import/ignore": [
        "node_modules",
        "\\.(coffee|scss|css|less|hbs|svg|json)$"
      ]
    },
    "rules": {
		"no-console": 0,
        "indent": [
            "error",
            4
        ],
		"no-mixed-spaces-and-tabs": [
	      "error",
	      "smart-tabs"
	    ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "never"
        ]
    }
};
