# Front End Dev Starter
.gitignored files
- dist
- node_modules
- hidden files except lint files
- todo.md
- zip files

## File Structure
    ├── src
    │   ├── build
    │   │   ├── config.js
    │   │   ├── webpack.config.js
    │   │   └── webpack.config.dev.js
    │   ├── fonts
    │   ├── images
    │   ├── js
    │   ├── scss
    │   │   ├── partials
    │   │   └── main.scss
    │   ├── views
    │   │   └── main.hbs
    │   └──index.hbs
    ├── .eslintrc.js
    ├── .gitignor
    ├── .sass-lint.yml
    ├── package.json
    ├── readme.md
    └── yarn.lock

## src/build/config.js

1. Entry defines locations for the main files. Other files are included via import or require statements. [Docs](https://webpack.js.org/concepts/#entry)

  ```javascript
  entry:  {
        main: [
            './index.hbs',//Entry files. Will follow the dependency tree from there
            './scss/main.scss',
            './js/main.js'
        ],
        otherJsFile: [
          './other.js'//will output a separate JS file and its dependancies
        ]
    }
  ```

## Commands
Commands are run via standard npm scripts. See package.json
### - yarn watch
- Starts a Webpack Hot Module Dev Server
  - Rebundles when files change
  - Holds files in memory... no files are actually outputted to dist folder except index.html

### - yarn build
- Builds and Minifies all Assets into their perspective folders
- Logs any Todo's
